### What is this repository for? ###

* Maven repository for the Narrar3 components. External users can get the Narrar3 components from here. This repository contains the SNAPSHOT versions.

### How do I get set up? ###

In the POM file use:

```
<repositories>
    <repository>
        <id>narrarRel</id>
        <name>narrarRelName</name>
        <releases>
            <enabled>true</enabled>
        </releases>
        <snapshots>
            <enabled>false</enabled>
        </snapshots>
        <url>https://api.bitbucket.org/1.0/repositories/narrar3/mvnrelease/raw/releases</url>
    </repository>
    <repository>
        <id>narrarSnapshot</id>
        <name>narrarSnapshotName</name>
        <releases>
            <enabled>true</enabled>
        </releases>
        <snapshots>
            <enabled>true</enabled>
        </snapshots>
        <url>https://api.bitbucket.org/1.0/repositories/narrar3/mvnsnapshot/raw/snapshots</url>
    </repository>
</repositories>
```

Using both sections won't hurt. You can use only the top one for release versions, the bottom half for snapshot versions.



### Who do I talk to? ###

jose